# Run the following to clean up some old indices

To build a new Docker image (normally you only have to do this once):

* Go to the directory containing the Dockerfile
* Execute: docker build -t teleena/curcuma .            (the dot means in this folder)
* Get a cup of coffee and wait...
* Wait a bit longer ...
* When everything is fine continue with executing the container itself :)


To run the container execute the following below, make sure to your LOCAL directories accordingly (so /User/Karsten/*** should be something like C:\whatever\directory\you\checkedout\git\into) :

```docker run -v /Users/Karsten/Customers/Teleena/Elastic/curcuma/actions/delete_indices.yml:/actions/delete_indices.yml -e ELASTIC_SERVER=elastic.teleena.local -e ELASTIC_PORT=9200 --rm teleena/curcuma --config /etc/curator.yml /actions/delete_indices.yml --dry-run```

